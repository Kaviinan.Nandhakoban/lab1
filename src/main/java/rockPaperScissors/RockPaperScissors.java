package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

	public static void main(String[] args) {
		/*
		 * The code here does two things: It first creates a new RockPaperScissors
		 * -object with the code `new RockPaperScissors()`. Then it calls the `run()`
		 * method on the newly created object.
		 */
		new RockPaperScissors().run();
	}

	Scanner sc = new Scanner(System.in);
	int roundCounter = 1;
	int humanScore = 0;
	int computerScore = 0;
	List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

	public void run() {
		
		while (true) {

			System.out.println("Let's play round " + roundCounter + "!");
	
			String playerMove = readInput("Your choice (Rock/Paper/Scissors)?");
			while(!rpsChoices.contains(playerMove)){
			    System.out.println("I do not understand " + playerMove + ". Could you try again?");
			    playerMove = readInput("Your choice (Rock/Paper/Scissors)?");
			}
	
			Random random = new Random();
			int randomNumber = random.nextInt(3);
	
			String computerMove;
			if (randomNumber == 0) {
				computerMove = "rock";
			} else if (randomNumber == 1) {
				computerMove = "paper";
			} else {
				computerMove = "scissors";
			}
	
			if (computerMove.equals(playerMove)) {
				System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!");
	
			} else if (wins(playerMove, computerMove)) {
				System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
				humanScore++;
			} else {
				System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
			
				computerScore++;
			}
			
			
			System.out.println("Score: human " + humanScore + " computer " + computerScore);
			
			
			String s = readInput("Do you wish to continue playing? (y/n)?");
			if(s.equals("n")) {
				break;
			} else 
				roundCounter++;
		}
		System.out.println("Bye bye :)");

	}

	boolean wins(String playerMove, String computerMove) {
		if (playerMove.equals("rock")) {
			return computerMove.equals("scissors");
		} else if (playerMove.equals("paper")) {
			return computerMove.equals("rock");
		} else {
			return computerMove.equals("paper");
		}
	}

	/**
	 * Reads input from console with given prompt
	 * 
	 * @param prompt
	 * @return string input answer from user
	 */
	public String readInput(String prompt) {
		System.out.println(prompt);
		String userInput = sc.next();
		return userInput;
	}

}